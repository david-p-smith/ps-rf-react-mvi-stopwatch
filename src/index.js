import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import { updateExpression } from '@babel/types';

let model = {
    running: false,
    time: 0
};

const view = (model) => {
    let minutes = Math.floor(model.time / 60);
    let seconds = model.time - (minutes * 60);
    let secondsFormatted = `${seconds < 10 ? '0' : ''}${seconds}`;
    let handler = (event) => {
        model = update(model, model.running ? 'STOP' : 'START');
    };
    return <div><p>{minutes}:{secondsFormatted}</p>
        <button onClick={handler}>{model.running ? 'Stop' : 'Start'}</button>
    </div>;
};

let intents = {
    TICK: 'TICK',
    START: 'START',
    STOP: 'STOP',
    RESET: 'RESET'
};

const update = (model, intent) => {
    const updates = {
        'TICK': (model) => Object.assign(model, { time: model.time + (model.running ? 1 : 0) }),
        'START': (model) => Object.assign(model, { running: true }),
        'STOP': (model) => Object.assign(model, { running: false })
    };
    return updates[intent](model);
};

const render = () => {
    ReactDOM.render(view(model), document.getElementById('root'));
};
render();

setInterval(() => {
    model = update(model, 'TICK');
    render();
}, 1000);


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
